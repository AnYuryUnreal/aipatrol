﻿

#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
#include <stdlib.h>
using namespace std;

struct SerialiseHandler //Структура для сериализации. Сохраняет минимально необходимый набор данных.
{   
    string Data;
    int RandElNum = -1;
};

class ListNode
{
    public:
    ListNode* Prev = NULL;
    ListNode* Next = NULL;
    ListNode* Rand = NULL; // произвольный элемент внутри списка
    string Data = "";
    


    ListNode(ListNode* PrevEl, string DataToStore) //Конструктор с сохранением указателя на предыдущий элемент
    {
        Data = DataToStore;
        Prev = PrevEl;
    }
    
};

class ListRand
{
    public:
    ListNode* Head = NULL;
    ListNode* Tail = NULL;
    int Count = 0;

    ListRand() {} //Конструктор по-умолчанию
    ListRand(int Len) //Генерация списка заданной длины
    {
        ListNode *PrevGen = NULL, *Buffer = NULL;
        
        Count = Len;
        
        for (int i = 0; i < Count; i++) //Создание основных элементов и связей
        {   
            Buffer = MakeNode(PrevGen,GenerateRandomString());
            if(PrevGen!=NULL)
            PrevGen->Next=Buffer;
            if (i == 0) Head = Buffer;
            if (i == Count)
              Tail = Buffer;
            else
              PrevGen = Buffer;
        }
        PrevGen = NULL;
        delete PrevGen;
        Buffer = Head;
        for (int i = 0; i < Count; i++) //Создание случайных связей
        {
            CreateRandomLink(Buffer, Count - i);
            Buffer = Buffer->Next;
        }
    }
    string GenerateRandomString()
    {
        //char alpha[26] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n','o', 'p', 'q', 'r', 's', 't', 'u','v', 'w', 'x', 'y', 'z' };
        string RandomString = "";
        for (int i = 0; i < 16; i++)
        {
            RandomString.append(1u, 'a' + rand() % 26);
        }
    
        return RandomString;
    }
    ListNode* MakeNode(ListNode* Parent, string DataToStore) //Создание элемента и передача в него необходимых данных
    {
         
        ListNode* GeneratedNode = new ListNode(Parent, DataToStore);
        return GeneratedNode;
    }

    void CreateRandomLink(ListNode *LinkOwner, int StepsLeft) 
    {   
        if (LinkOwner->Rand == NULL)
        {
            ListNode* Candidate = NULL;
            do
            { 
                Candidate = LinkOwner;
                if (StepsLeft != 0)
                {
                    int StepsAmount = rand() % StepsLeft;
                    if (StepsAmount > 0)
                    {
                        for (int i = 1; i <= StepsAmount; i++)
                        {
                            Candidate = Candidate->Next;
                        }
                    }
                }
            } while (Candidate->Rand != NULL);
            LinkOwner->Rand = Candidate;
            Candidate->Rand = LinkOwner;
        }
    
    }
    SerialiseHandler* MakeArrayFromList() //Преобразование двусвязного списка в массив для сохранения.
    {
        if (Count > 0) //Защита от создания нулевого массива
        {

            SerialiseHandler* Array = new SerialiseHandler[Count];
            ListNode* Output = Head;

            for (int i = 0; i < Count; i++)
            {
                //cout << "Pointer=" << Output << "|" << "Data=" << Output->Data << "|" << "Prev=" << Output->Prev << "|" << "Next=" << Output->Next << "|" << "Rand=" << Output->Rand << endl;
                Array[i].Data = Output->Data;
                if (Array[i].RandElNum == -1) //Проверка на наличие более ранней записи связи;
                {
                    if (Output->Rand == Output) //Если элемент связан сам с собой
                        Array[i].RandElNum = i;
                    else
                    { //Поиск в двух направлениях
                        int p = 0, n = 0;
                        ListNode* PrevBuffer = Output->Prev, * NextBuffer = Output->Next;
                        p++; n++;
                        do
                        {
                            if (PrevBuffer != NULL)
                            {
                                if (PrevBuffer == Output->Rand)
                                {
                                    n = -1;
                                    break;
                                }
                                else
                                {
                                    PrevBuffer = PrevBuffer->Prev;
                                    p++;
                                }
                            }
                            else p = -1;

                            if (NextBuffer != NULL)
                            {
                                if (NextBuffer == Output->Rand)
                                {
                                    p = -1;
                                    break;
                                }
                                else
                                {
                                    NextBuffer = NextBuffer->Next;
                                    if (NextBuffer != NULL) n++;
                                }
                            }
                            else n = -1;

                        } while (p != -1 or n != -1);//Закончить перебор элементов, если достигли краёв.
                        PrevBuffer = NULL;
                        delete PrevBuffer;
                        NextBuffer = NULL;
                        delete NextBuffer;
                        if (p != -1)  Array[i].RandElNum = i - p; //Если связанный элемент находится раньше
                        else
                            if (n != -1) //Если связанный элемент находится позже
                            {

                                Array[i].RandElNum = i + n;
                                if (i+n<Count) //Валидация индекса
                                Array[i + n].RandElNum = i;//Передача связанному элементу номера "приятеля" (Поможет практически в 2 раза уменьшить количество вызовов поиска)

                            }
                            else Array[i].RandElNum = -1; //Если связанный элемент не находится
                    }
                }
                Output = Output->Next;
            }
            return Array;
        }
    }

    void ScreenOutput(SerialiseHandler* Array)
    {
        for (int i = 0; i < Count; i++)
        {
            cout << "Pointer=" << i << "|" << "Data=" << Array[i].Data << "|"<< "Rand=" << Array[i].RandElNum << endl;
        }
    }
    
    void Serialize(ofstream &s)
    {
        SerialiseHandler* Array = MakeArrayFromList();
        if (s.is_open())
        {
            s << Count << endl;
            for (int i = 0; i < Count; i++)
            {
                s << Array[i].Data << "|" << Array[i].RandElNum << endl;

            }
            cout << "RawData" << endl;
            RawScreenOutput();
        }
        else cout << "Can't create file" << endl;
    }

    SerialiseHandler CutDataFromString(string Source, string Delimiter) //Создание элемента промежуточного массива из строковой переменной с использованием разделителя
    {
        SerialiseHandler ArrayElem;
        string Cut[2];
        for (int i = 0; i < 2; i++)
        {
            size_t Position = 0;
            Position = Source.find(Delimiter);
            Cut[i] = Source.substr(0, Position);
            Source.erase(0, Position + Delimiter.length());
        }
        ArrayElem.Data = Cut[0];
        ArrayElem.RandElNum = stoi(Cut[1]);
        return ArrayElem;
    }
    
    void ArrayToList(SerialiseHandler* Array)
    {  
        ListNode* PrevGen = NULL, * Buffer = NULL;
        for (int i = 0; i < Count; i++) //Восоздание основных элементов и связей
        {
            Buffer = MakeNode(PrevGen,Array[i].Data);
            if (PrevGen != NULL)
                PrevGen->Next = Buffer;
            if (i == 0) Head = Buffer;
            if (i == Count)
                Tail = Buffer;
            else
                PrevGen = Buffer;
        }
        PrevGen = NULL;
        delete PrevGen;
        Buffer = Head;
        for (int i = 0; i < Count; i++)
        {
            LoadRandomLink(Buffer, i ,Array[i].RandElNum);
            Buffer = Buffer->Next;
        }
    }

    void LoadRandomLink(ListNode* LinkOwner, int OwnerId, int BuddyId) //Воссоздание случайной связи
    {
        if (LinkOwner->Rand == NULL)
        {
            if (BuddyId == OwnerId)
                LinkOwner->Rand = LinkOwner;
            else
            {
                int Step = 0;
                ListNode* Candidate = LinkOwner;
                if (BuddyId < OwnerId) Step = -1; else Step = 1;
                for (int i = OwnerId; i != BuddyId; i = i + Step)
                {
                    if (Step == 1) Candidate = Candidate->Next; else Candidate = Candidate->Prev;
                }
                LinkOwner->Rand = Candidate;
                Candidate->Rand = LinkOwner;
            }      
        }
    }
    void RawScreenOutput() 
    {
        ListNode* Output = Head;

        for (int i = 0; i < Count; i++)
        {
         cout << "Pointer=" << Output << "|" << "Data=" << Output->Data << "|" << "Prev=" << Output->Prev << "|" << "Next=" << Output->Next << "|" << "Rand=" << Output->Rand << endl;
         Output = Output->Next;
        }
    }
    void Deserialize(ifstream &s)
    {
        if (s.is_open())
        {

            s >> Count;
            cout << Count <<" items in list" << endl;
            SerialiseHandler* Array = new SerialiseHandler[Count];
            for (int i = 0; i < Count; i++)
            {
                string LineBuffer = "";
                string Delimiter = "|";
                s >> LineBuffer;
                Array[i] = CutDataFromString(LineBuffer, Delimiter);
            }
            ArrayToList(Array); //Вызов заполнения списка элементами массива
            ScreenOutput(MakeArrayFromList());//Вывод загруженных данных в читаемом виде
            cout << "RawData" << endl;
            RawScreenOutput();//Вывод сырых данных
        }
        else cout << "No data found" << endl;

    }
};

int main()
{
    ifstream fin;
    ofstream fout;
    srand(time(NULL));
    int Decision = 0;
    do
    {
        cout << "1 = Generate and Save, 2 = Load and Show" << endl;
        cin >> Decision;
        if (cin.fail())
        {
            cin.clear();
            while (cin.get() != '\n');
            Decision = -1;
        }
    } while (Decision != 1 and Decision != 2);
    if (Decision == 1)
    {
        int Len = 0;
        cout << "Enter number of items in list" << endl;
        cin >> Len;
        if (cin.fail() or Len<1)
        {
            cin.clear();
            while (cin.get() != '\n');
            cout << "Invalid input. Generating 5 items list." << endl;
            Len = 5;
        }
        ListRand* a = new ListRand(Len);
        fout.open("Parse.txt");
        a->Serialize(fout);
        fout.close();
    }
    else 
    {
        ListRand* a = new ListRand;
        fin.open("Parse.txt");
        a->Deserialize(fin);
        fin.close();
    }
    std::system("Pause");
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
